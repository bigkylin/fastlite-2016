package com.darryring.fast.activitys;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;

import com.darryring.fast.R;
import com.darryring.fast.services.ChatService;
import com.darryring.fast.services.FastLiteService;
import com.darryring.fast.theme.AndroidFastTheme;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libcore.Fast;
import com.darryring.libcore.base.BaseActivity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends BaseActivity {

    private final Handler mHideHandler = new Handler();
    private String TAG="SplashActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.getInstance(AndroidFastTheme.class);
        FastTheme.fastTheme.init(this);

//        Fast.fastTheme.setupTheme(this, ThemeEntity.TYPE_SPLASH_THEME);
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate...");
        Resources _res = getResources();
        setContentView(R.layout.activity_splash);
        //------------------------------------------------
        startService(new Intent(this, ChatService.class));
        startService(new Intent(this, FastLiteService.class));
        //---------------------------------------------------
        DisplayMetrics _dms =  _res.getDisplayMetrics();
        Fast.logger.i(TAG,"WIDTH:"+_dms.widthPixels);
        Fast.logger.i(TAG,"HEIGHT:"+_dms.heightPixels);
        if(Fast.kvCache.readFromFileCache(LoginActivity.SAVE_LOGIN_STATE)!=null){
            Intent mIm = new Intent(SplashActivity.this,HomeActivity.class);
            startActivity(mIm);
            finish();
            return;
        }
        mHideHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mIm = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(mIm);
                finish();
            }
        },3000);


    }



}
