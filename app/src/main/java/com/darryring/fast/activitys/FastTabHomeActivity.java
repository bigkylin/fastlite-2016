package com.darryring.fast.activitys;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.darryring.fast.R;
import com.darryring.fast.adapter.HomeAdapter;
import com.darryring.fast.adapter.data.TabEntity;
import com.darryring.fast.fragment.BaseFragment;
import com.darryring.fast.fragment.ContactFragment;
import com.darryring.fast.fragment.MenuListFragment;
import com.darryring.fast.fragment.SessionFragment;
import com.darryring.fast.fragment.UserFragment;
import com.darryring.fast.theme.FastTheme;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.libmodel.entity.theme.ThemeEntity;
import com.darryring.libview.FastTabLayout;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.flyco.tablayout.utils.UnreadMsgUtils;
import com.flyco.tablayout.widget.MsgView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FastTabHomeActivity extends AppCompatActivity {

    private String[] mTitles = {"聊天", "通信录", "功能", "账户"};

    private int menuIconIds[] = {com.darryring.libres.R.drawable.libres_item_chat_drawable,
            com.darryring.libres.R.drawable.libres_item_contact_drawable,
            com.darryring.libres.R.drawable.libres_item_app_drawable,
            com.darryring.libres.R.drawable.libres_item_preson_drawable};


    FastTabLayout mFastTabLayout;
    ViewPager mViewPager;
    HomeAdapter mHomeAdapter;

    ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    ThemeEntity mThemeEntity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mThemeEntity = FastTheme.fastTheme.theme();
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_tab_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mFastTabLayout = (FastTabLayout) findViewById(R.id.tabbar);
        //
        List<BaseFragment> fragments = new ArrayList<>();
        fragments.add(new SessionFragment("聊天"));
        fragments.add(new ContactFragment("通信录"));
        fragments.add(new MenuListFragment("功能"));
        fragments.add(new UserFragment("账户"));
        //
        //--------------------------------------------
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], menuIconIds[i], menuIconIds[i]));
        }
        //
        mHomeAdapter = new HomeAdapter(getSupportFragmentManager(),fragments);
        //----------------------------------------------------------
        if(FastTheme.fastTheme.theme()!=null){
            TypedArray ta = getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
            int _defaultColor =getResources().getColor(R.color.libres_menu_text_normal);
            int _selectColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);
            //设置tab textview
            ColorStateList colorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
            mFastTabLayout.setTabTextColorStateList(colorStateList);

            //设置tab icon
            ColorStateList iconColorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
            mFastTabLayout.setTabIconColorStateList(iconColorStateList);
            //
            ta.recycle();
        }else{
            mFastTabLayout.setTabTextColorStateList(getResources().getColorStateList(R.color.libres_menu_item_textcolor));
        }
        //----------------------------------------------------------
        mViewPager.setAdapter(mHomeAdapter);
        mViewPager.setOffscreenPageLimit(fragments.size());
        //==========================================================

        //==========================================================
        setupTabBar();
        //两位数
        mFastTabLayout.showMsg(0, 55);
        mFastTabLayout.setMsgMargin(0, -5, 5);

        //三位数
        mFastTabLayout.showMsg(1, 100);
        mFastTabLayout.setMsgMargin(1, -5, 5);

        //设置未读消息红点
        mFastTabLayout.showDot(2);
        MsgView rtv_2_2 = mFastTabLayout.getMsgView(2);
        if (rtv_2_2 != null) {
            UnreadMsgUtils.setSize(rtv_2_2, dp2px(7.5f));
        }

        //设置未读消息背景
        mFastTabLayout.showMsg(3, 5);
        mFastTabLayout.setMsgMargin(3, 0, 5);
        MsgView rtv_2_3 = mFastTabLayout.getMsgView(3);
        if (rtv_2_3 != null) {
            rtv_2_3.setBackgroundColor(Color.parseColor("#6D8FB0"));
        }
    }

    private void setupTabBar() {
        mFastTabLayout.setTabData(mTabEntities);
        mFastTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                    mViewPager.setCurrentItem(position, false);
            }

            @Override
            public void onTabReselect(int position) {
                if (position == 0) {
                    mFastTabLayout.showMsg(0, mRandom.nextInt(100) + 1);
                    UnreadMsgUtils.show(mFastTabLayout.getMsgView(0), mRandom.nextInt(100) + 1);
                }
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                mFastTabLayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    Random mRandom = new Random();
    protected int dp2px(float dp) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
