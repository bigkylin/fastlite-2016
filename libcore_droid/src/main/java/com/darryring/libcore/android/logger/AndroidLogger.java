package com.darryring.libcore.android.logger;

import android.util.Log;

import com.darryring.libcore.logger.Logger;

/**
 * Created by hljdrl on 15/12/11.
 */
public class AndroidLogger implements Logger {

    private boolean info;
    private boolean debug;
    private boolean v;
    private boolean error;
    public AndroidLogger(boolean i,boolean d,boolean v,boolean e){
        info = i;
        debug = d;
        this.v = v;
        error =e;
    }
    /**
     * @param tag
     * @param msg
     */
    @Override
    public void i(String tag, String msg) {
        if(info) {
            Log.i(tag, msg);
        }
    }

    /**
     * @param tag
     * @param msg
     */
    @Override
    public void d(String tag, String msg) {
        if(debug) {
            Log.d(tag, msg);
        }
    }

    /**
     * @param tag
     * @param msg
     */
    @Override
    public void v(String tag, String msg) {
        Log.v(tag,msg);
    }

    /**
     * @param tag
     * @param msg
     */
    @Override
    public void e(String tag, String msg) {
        if(error) {
            Log.e(tag, msg);
        }
    }

    /**
     * @param tag
     * @param msg
     */
    @Override
    public void e(String tag, Throwable msg) {
        if(error) {
            Log.e(tag, "", msg);
        }
    }
}
