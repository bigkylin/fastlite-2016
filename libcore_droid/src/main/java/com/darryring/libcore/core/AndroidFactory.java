package com.darryring.libcore.core;

import android.content.Context;

import com.darryring.libcore.Fast;
import com.darryring.libcore.android.cache.FileDiskCache;
import com.darryring.libcore.android.cache.MemoryCache;
import com.darryring.libcore.android.file.AndroidFiles;
import com.darryring.libcore.android.http.AndroidOkHttp;
import com.darryring.libcore.android.logger.AndroidLogger;
import com.darryring.libcore.android.media.AndroidMedia;
import com.darryring.libcore.android.task.AndroidTask;
import com.darryring.libcore.android.util.AndroidNetWork;

/**
 * Created by hljdrl on 16/6/1.
 */
public final class AndroidFactory {

    public static void  newFastFactory (Context _context){
        if (Fast.logger == null) {
            Fast.logger = new AndroidLogger(true, true, true, true);
        }
        if (Fast.files == null) {
            Fast.files = new AndroidFiles();
        }
        if (Fast.task == null) {
            Fast.task = new AndroidTask();
        }
        if (Fast.http == null) {
            Fast.http = new AndroidOkHttp(_context);
        }
        if (Fast.kvCache == null) {
            Fast.kvCache = new MemoryCache();
        }
        if (Fast.diskCache == null) {
            Fast.diskCache = new FileDiskCache(_context);
        }
        if (Fast.media == null) {
            Fast.media = new AndroidMedia(_context);
        }
        if(Fast.network==null) {
            Fast.network = new AndroidNetWork(_context);
        }
    }
}
