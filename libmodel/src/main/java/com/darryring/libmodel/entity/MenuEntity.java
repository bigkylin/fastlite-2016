package com.darryring.libmodel.entity;

import android.content.Intent;

/**
 * Created by hljdrl on 16/5/23.
 */
public class MenuEntity {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    private String name="";
    private int icon=0;
    private Intent intent;
}
